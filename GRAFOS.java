/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafos;

/**
 *
 * @author hitzu
 */
public class GRAFOS {
    

    public static Grafo getCities() {
        Nodo camp = new Nodo("Campeche");
        Nodo calk = new Nodo("Calkini");
        Nodo cand = new Nodo("Candelaria");
        Nodo pali = new Nodo("Palizada");
        Nodo carm = new Nodo("Carmen");
        Nodo tena = new Nodo("Tenabo");
        Nodo cham = new Nodo("Champoton");
 
        camp.addEdge(new Edge(camp, carm, 206));
        calk.addEdge(new Edge(calk, tena, 47.7));
        cand.addEdge(new Edge(cand, pali, 146));
        pali.addEdge(new Edge(pali, camp, 355));
        carm.addEdge(new Edge(carm, calk, 295));
        tena.addEdge(new Edge(tena, cham, 105));
        cham.addEdge(new Edge(cham, cand, 150));
 
        Grafo grafo = new Grafo ();
        grafo.addNode(camp);
        grafo.addNode(calk);
        grafo.addNode(cand);
        grafo.addNode(pali);
        grafo.addNode(carm);
        grafo.addNode(tena);
        grafo.addNode(cham);

        return grafo;
    }
    public static void main(String[] args) {
             String[] CiOr = {
            "CAMPECHE","CALKINI", "CANDELARIA", "PALIZADA","CARMEN","TENABO",
            "CHAMPOTON"};
        
        String[] CiDes = {
            "CARMEN","TENABO","PALIZADA", "CAMPECHE","CALKINI",  
            "CHAMPOTON", "CANDELARIA"};
        
        float[] Distancia = { 
            206, 47.7f, 176, 355, 295, 105, 105, 150};
        
         for (int i = 0; i < CiOr.length; i++) {
             System.out.print("Ciudad de Origen" + " " + CiOr[i]+ "  ");
             System.out.println("Ciudad de Destino" + " " + CiDes[i]+ " " +  " distancia en km "+  " " +  Distancia[i]);
         }
        Grafo grafo = getCities();
        System.out.println(grafo);
            
    }
    } 
