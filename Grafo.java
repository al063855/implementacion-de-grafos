package grafos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hitzu
 */
public class Grafo {
     private List<Nodo> nodes;

    public void addNode(Nodo nodo) {
        if (nodes == null) {
            nodes = new ArrayList<>();
        }
        nodes.add(nodo);
    }
    
    public List<Nodo> getNodes() {
        return nodes;
    }

    @Override
    public String toString() {
        return "Graph{" + "Nodo=" + nodes + '}';
    }
    
   
    
}
